import { Component, OnInit } from '@angular/core';
import { Group } from '../shared/gitlab/group';
import { GroupService } from '../shared/gitlab/group.service';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  group: Group;

  constructor(private gs: GroupService) { }

  ngOnInit() {
    this.gs.getGroup().subscribe(res => this.group = res);
  }

}
