import { Component, OnInit } from "@angular/core";
import { Group } from "../shared/gitlab/group";
import { GroupService } from "../shared/gitlab/group.service";
import { Project } from "../shared/gitlab/project";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  group: Group;

  constructor(private gs: GroupService) {}

  ngOnInit() {
    this.gs.getGroup().subscribe(res => (this.group = res));

    console.log(this.group.projects);
  }
}
