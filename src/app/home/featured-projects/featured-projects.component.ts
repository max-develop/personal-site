import { Component, OnInit, Input } from "@angular/core";
import { Project } from "src/app/shared/gitlab/project";
@Component({
  selector: "app-featured-projects",
  templateUrl: "./featured-projects.component.html",
  styleUrls: ["./featured-projects.component.scss"]
})
export class FeaturedProjectsComponent implements OnInit {

  @Input() featuredProjects: Project[];

  constructor() {
  }

  ngOnInit(): void {}
}
