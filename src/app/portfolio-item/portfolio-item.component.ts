import { Component, OnInit, Input, NgModule } from "@angular/core";
import { Project } from "../shared/gitlab/project";
import { Modal } from "materialize-css";
import { Router } from "@angular/router";

@Component({
  selector: "app-portfolio-item",
  templateUrl: "./portfolio-item.component.html",
  styleUrls: ["./portfolio-item.component.scss"]
})
export class PortfolioItemComponent implements OnInit {
  @Input() project: Project;

  title: String;
  link: String;
  description: String;

  constructor(private router: Router) {}

  ngOnInit() {
    this.setTitle();
    this.setLink();
    this.getDescription();
  }

  setTitle() {
    this.title = this.project.name;
  }

  setLink() {
    this.link = this.project.web_url;
  }

  getDescription() {
    this.description = this.project.description;
  }

  isEmpty() {
    return this.description === "";
  }

  showDetails() {
    this.router.navigateByUrl("/portfolio/" + this.project.id);
  }
}
