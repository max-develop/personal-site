import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { GroupService } from "../shared/gitlab/group.service";

@Component({
  selector: "app-portfolio-detail",
  templateUrl: "./portfolio-detail.component.html",
  styleUrls: ["./portfolio-detail.component.scss"]
})
export class PortfolioDetailComponent implements OnInit {
  readme: string;

  constructor(private route: ActivatedRoute, private gs: GroupService) {}

  ngOnInit(): void {
    const params = this.route.snapshot.paramMap;
    this.gs
      .getREADME(params.get("id"))
      .subscribe(res => (this.readme = atob(res.content)));
  }
}
