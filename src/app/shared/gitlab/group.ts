import { Project } from './project';

export interface Group {
    id: number;
    web_url: string;
    name: string;
    path: string;
    description: string;
    visibility: string;
    share_with_group_lock: boolean;
    require_two_factor_authentication: boolean;
    two_factor_grace_period: number;
    project_creation_level: string;
    auto_devops_enabled?: any;
    subgroup_creation_level: string;
    emails_disabled?: any;
    lfs_enabled: boolean;
    avatar_url?: any;
    request_access_enabled: boolean;
    full_name: string;
    full_path: string;
    parent_id?: any;
    projects: Project[];
    shared_projects: any[];
    ldap_cn?: any;
    ldap_access?: any;
    shared_runners_minutes_limit?: any;
    extra_shared_runners_minutes_limit?: any;
}
