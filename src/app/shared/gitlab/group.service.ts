import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';

import { Group } from './group';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  private api = 'https://gitlab.com/api/v4/groups/max-develop'
  private apiFiles = 'https://gitlab.com/api/v4/projects/'

  constructor(private http: HttpClient) { }

  getGroup(): Observable<Group> {
    return this.http.get<Group>(this.api)
      .pipe(
        retry(3)
      );
  }

  getREADME(id: string){
    return this.http.get<any>(`${this.apiFiles}${id}/repository/files/README.md?ref=master`)
      .pipe(
        retry(3)
      );
  }
}
