import { Feed } from './feed';
import { Item } from './item';

export interface Blog {
    status: string;
    feed: Feed;
    items: Item[];
}
