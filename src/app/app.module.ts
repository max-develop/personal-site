import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PortfolioPreviewComponent } from './portfolio-preview/portfolio-preview.component';
import { ResumeeComponent } from './resumee/resumee.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { PortfolioItemComponent } from './portfolio-item/portfolio-item.component';
import { HomeComponent } from './home/home.component';
import { FeaturedProjectsComponent } from "./home/featured-projects/featured-projects.component";
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    PortfolioPreviewComponent,
    ResumeeComponent,
    PortfolioComponent,
    PortfolioItemComponent,
    HomeComponent,
    FeaturedProjectsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
